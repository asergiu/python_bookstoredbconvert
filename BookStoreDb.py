__author__ = 'dadi'
from openpyxl import load_workbook
from openpyxl import Workbook
from openpyxl.worksheet.iter_worksheet import IterableWorksheet
from openpyxl.cell.read_only import ReadOnlyCell
import sqlite3
import os
import sys

def get_header(row):
    colcount=0
    header=()
    for c in row:
        if not c.value == None:
            header += (c.value,)
        else:
            break
    return header

def stripchars(mystr, strip):
    dict=str.maketrans('a','a',strip)
    return mystr.translate(dict)


def createDB(ws):
    rcount=0
    nullrows=0
    dbfile=stripchars(ws.title,'/ .;,;')
    tablename=dbfile
    dbfile+='.db'

    try:
        if os.path.isfile(dbfile):
            os.remove(dbfile)
    except OSError:
        print('Cannot clean the db file '+dbfile)

# def isEmpty(row):
#     isEmpty=True
#     for c in row:
#         if

    conn=sqlite3.connect(dbfile)
    str_cmd='create virtual table '+tablename+' using fts3('
    list_values=[]
    for row in ws.rows:
        rcount+=1
        print(rcount,end='\r')
        if rcount == 1:
            header=get_header(row)
            placeholder=''
            for rawcolname in header:
                colname=stripchars(rawcolname,'/ .;,;')
                # print(colname)
                str_cmd+=colname+', '
                placeholder+='?,'
            str_cmd=str_cmd.rstrip(',')
            str_cmd+=')'
            placeholder=placeholder.rstrip(',')

            # print()
            # print(str_cmd)
            conn.execute(str_cmd)
            conn.commit()
        else:
            str_cmd='insert into '+tablename+' values('+placeholder+')'
            row_value=()
            for i in range(0,len(header)):
                row_value+=(row[i].value,)
                # print(row[i].value, end='#')
            list_values+=[row_value]
            # print()
            # print(str_cmd)
            # print(list_values)
            if len(list_values)>300:
                conn.executemany(str_cmd,list_values)
                # conn.commit()
                list_values=[]

    if len(list_values)>0:
        conn.executemany(str_cmd,list_values)
    conn.commit()
    conn.close()
    return

if len(sys.argv) <2:
    print("Usage: "+sys.argv[0]+" XLS filename")
    print("\t Will take all sheets and generate a separate .db file for each sheet")
    sys.exit(-1)
    
xls=sys.argv[1]
wb=load_workbook(filename=xls,read_only=True)

# c = conn.cursor()
# c.execute('pragma compile_options')
# for row in c:
#     print(row)
for ws in wb:
    print(ws.title)
    createDB(ws)
    print()
